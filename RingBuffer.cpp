/*
 * RingBuffer.cpp
 *
 *  Created on: 2022年12月24日
 *      Author: hello
 */

#include "RingBuffer.h"

RingBuffer::RingBuffer(uint32_t size){
	wpos = 0;
	rpos = 0;
	this->size = size + 1;
	payload = new uint8_t[((this->size + 5) >> 2) << 2];
}

RingBuffer::~RingBuffer() {
	delete[] payload;
}

void RingBuffer::rbmemcpy(void* dst, const void* src, uint32_t sz)
{
	char *d = (char *) dst;
	const char *s = (const char *) src;
	if(((int)d) % 4 == 0 && (((int)s) % 4) == 0)  /* 源地址与目标地址都对其4字节的情况下那么使用4字节拷贝的方式 */
	{
		while (sz >= sizeof(int))
		{
			*(int *) d = *(int *) s;
			d += sizeof(int);
			s += sizeof(int);
			sz -= sizeof(int);
		}
	}
	while (sz--)
	{
		*d++ = *s++;
	}
}

uint32_t RingBuffer::read(void *buf, uint32_t length)
{
	uint8_t* p = (uint8_t *) buf;
	uint32_t offset = 0, nused = 0, nread = 0;

	if (!length)
		return 0;

	if (!(nused = getUsed()))
		return 0;

	nread = nused >= length ? length : nused;
	offset = size - rpos;

	if (offset >= nread)
	{
		rbmemcpy(p, payload + rpos, nread);
		rpos += nread;
	}
	else
	{
		rbmemcpy(p, payload + rpos, offset);
		rbmemcpy(p + offset, payload, nread - offset);
		rpos = nread - offset;
	}
	return nread;
}

uint32_t RingBuffer::write(const void* buf, uint32_t length)
{
	const uint8_t* p = (const uint8_t *) buf;
	uint32_t offset = 0, nwrite = 0, nfree = 0;

	if (!length)
		return 0;

	if (!(nfree = getFree()))
		return 0;

	nwrite = nfree >= length ? length : nfree;
	offset = size - wpos;

	if (offset >= nwrite)
	{
		rbmemcpy(payload + wpos, p, nwrite);
		wpos += nwrite;
	}
	else
	{
		rbmemcpy(payload + wpos, p, offset);
		rbmemcpy(payload, p + offset, nwrite - offset);
		wpos = nwrite - offset;
	}
	return nwrite;
}

bool RingBuffer::write(uint8_t c)
{
	if(isFull())
	{
		return false;
	}
	wpos %= size;
	payload[wpos++] = c;
	return true;
}

bool RingBuffer::read(uint8_t* c)
{
	if(isEmpty())
	{
		return false;
	}
	rpos %= size;
	if(c) *c = payload[rpos++];
	return true;
}

bool RingBuffer::isFull()
{
	return (rpos == (wpos + 1) % size) ? true : false;
}

bool RingBuffer::isEmpty()
{
	return rpos == wpos ? true : false;
}

uint32_t RingBuffer::getUsed()
{
	int len = wpos - rpos;
	return len >= 0 ? len : (size + len);
}

uint32_t RingBuffer::getFree()
{
	int len = wpos - rpos;
	return len >= 0 ? (size - 1 - len) : (((int)-1) - len);
}

uint32_t RingBuffer::getSize()
{
	return this->size - 1;
}

void RingBuffer::clear()
{
	wpos = rpos;
}

// 撤销上次读取，慎用！
void RingBuffer::undoRead(uint32_t len)
{
    int tmp = rpos - len;
    rpos = tmp < 0 ? ((uint32_t)(tmp + size)) : ((uint32_t)tmp);
}

// 撤销上次写入，慎用！
void RingBuffer::undoWrite(uint32_t len)
{
    int tmp = wpos - len;
    wpos = tmp < 0 ? ((uint32_t)(tmp + size)) : ((uint32_t)tmp);
}
