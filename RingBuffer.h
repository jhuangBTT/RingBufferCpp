/*
 * RingBuffer.h
 *
 *  Created on: 2022��12��24��
 *      Author: hello
 */

#ifndef RINGBUFFER_H_
#define RINGBUFFER_H_

#include <stdint.h>

class RingBuffer {
public:
	explicit RingBuffer(uint32_t size);
	virtual ~RingBuffer();

	uint32_t write(const void* buf, uint32_t len);
	bool write(uint8_t c);
	uint32_t read(void* buf, uint32_t len);
	bool read(uint8_t* c);
	bool isEmpty(void);
	bool isFull(void);
	uint32_t getSize(void);
	uint32_t getFree(void);
	uint32_t getUsed(void);
	void clear(void);
	void undoWrite(uint32_t len);
	void undoRead(uint32_t len);

private:
	uint32_t wpos, rpos, size;
	uint8_t* payload;
	void rbmemcpy(void* dst, const void* src, uint32_t sz);
};

#endif /* RINGBUFFER_H_ */
