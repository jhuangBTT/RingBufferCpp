/*
 * main.c
 *
 *  Created on: 2022��12��24��
 *      Author: hello
 */

#include "RingBuffer.h"
#include <stdio.h>

int main()
{
	uint8_t tmp[64]={0};

	RingBuffer buf(10);

	// д
	buf.write("helloworld", 10);

	// ��
	buf.read(tmp, 10);

	printf("%s\n", tmp);

	// д
	buf.write('#');

	// ��
	buf.read(&tmp[0]);

	printf("%c \n", tmp[0]);

	return 0;
}
